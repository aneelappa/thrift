#client.py
import sys
sys.path.append('gen-py')

from thrift.transport import TSocket
from thrift.protocol import TCompactProtocol
from thrift.transport import TTransport

from cloudbash import FileManagementService
from cloudbash.ttypes import *

DEFAULT_HOST1 = 'localhost'
DEFAULT_HOST2 = 'localhost'
DEFAULT_PORT1 = '9000'
DEFAULT_PORT2 = '9001'

class Client():

	def __init__(self,host1,host2,port1,port2):
		self.host1 = host1
		self.host2 = host2
		self.port1 = int(port1)
		self.port2 = int(port2)
		return


	def run(self,src_file, dest_file,num_lines, reverse = False):
		if reverse:
			src_host = self.host2
			dest_host = self.host1
			src_port = self.port2
			dest_port = self.port1
		else:
			src_host = self.host1
			dest_host = self.host2
			src_port = self.port1
			dest_port = self.port2

		transport = TSocket.TSocket(src_host,src_port)
		transport = TTransport.TFramedTransport(transport)
		protocol = TCompactProtocol.TCompactProtocol(transport)
		client = FileManagementService.Client(protocol)
		try:
			transport.open()
		except Exception as e:
			print "Could not open connection"
			return

		request= MoveRequest(src_file,num_lines,dest_host,dest_port,dest_file)	

		try:
			client.moveLinesFromEnd(request)
			print 'Lines moved'
		except MoveException as e:
			print "MoveException"
			print e
			if e.code == ExceptionCode.DIRTY:
				print "Files inconsistent."
			return
		except Exception as e:
			print "Something unexpected"
			# We should check for consistency here too. 
			# Also we should add functions for correcting the inconsistency 

			#import traceback
			#traceback.print_exc()

client = Client(DEFAULT_HOST1,DEFAULT_HOST2,DEFAULT_PORT1,DEFAULT_PORT2)
client.run('dummy_1.txt','dummy_2.txt',5)