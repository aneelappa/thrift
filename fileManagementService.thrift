//fileManagementService.thrift

namespace py cloudbash

enum ExceptionCode{
	UNDEF=0,
	COULD_NOT_READ=1,
	COULD_NOT_WRITE =2,
	SRC_FILE_TOO_SMALL =3,
	DIRTY =4
}

struct MoveRequest{
	1: required string src_file_name,// The file which needs to be modified
	2: required i32 num_lines, // number of lines to be moved
	3: required string dest_host
	4: required i32 dest_port
	5: required string dest_file_name
}

struct WriteRequest{
	1: required string file_name,// destination file name
	2: required list<string> data
}

exception MoveException{
	1: required ExceptionCode code,
	2: optional string message
}

exception WriteException{
	1: required ExceptionCode code,
	2: optional string message
}


service FileManagementService{
	void moveLinesFromEnd(1:MoveRequest request) throws(1:MoveException e),
	void writeLinesAtStart(1:WriteRequest request) throws(1:WriteException e),
}
