client.py - Thrift client to connect with the file servers
server.py - Thrift file server
fileManagementHandler.py - Handler for managing the file according to client request
fileManagementService.thrift - Thrift file which defines the service interface
gen-py - generated python files
dummy_1.txt - test file 1
dummy_2.txt - test file 2
