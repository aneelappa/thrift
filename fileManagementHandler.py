#fileManagementHandler.py
import sys
import subprocess
sys.path.append('gen-py')

from cloudbash import FileManagementService
from cloudbash.ttypes import *

from thrift.transport import TSocket
from thrift.protocol import TCompactProtocol
from thrift.transport import TTransport


class FileManagementHandler(FileManagementService.Iface):

	def moveLinesFromEnd(self,request):
		#parse the request 
		src_file_name = request.src_file_name
		num_lines = request.num_lines
		dest_host = request.dest_host
		dest_port = request.dest_port
		dest_file_name = request.dest_file_name

		#print num_lines
		#print src_file_name
		#first read the lines from the source file 
		tail = subprocess.Popen(['tail','-n '+str(num_lines),src_file_name],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		data, err = tail.communicate()
		if err:
			raise MoveException(ExceptionCode.COULD_NOT_READ,"was unable to read the data")


		data = data.split('\n')
		lines_found = len(data)
		 
		if lines_found < num_lines:
			raise MoveException(ExceptionCode.SRC_FILE_TOO_SMALL,"Source file does not have enough lines")
		

		# Now create a client for the destination server to write to it. Client code seems to be repeated here. Try and move it out
		transport = TSocket.TSocket(dest_host,dest_port)
		transport = TTransport.TFramedTransport(transport)
		protocol = TCompactProtocol.TCompactProtocol(transport)
		client = FileManagementService.Client(protocol)
		
		try:
			transport.open()
		except Exception as e:
			print "Could not open connection"
			print e

		write_request = WriteRequest(dest_file_name,data)

		try:
			client.writeLinesAtStart(write_request)
		except WriteException as e:
			raise MoveException(ExceptionCode.COULD_NOT_WRITE,"Could not write to destination")
		except Exception as e:
			raise MoveException(ExceptionCode.UNDEF, "UNDEF")

		#Now that the data has been removed, remove it from this file
		
		#find number of lines in this file
		wc = subprocess.Popen(['wc','-l',src_file_name],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		len_tot,err_wc = wc.communicate()
		#print len_tot
		#print err
		if err_wc:
			print err_wc
			raise MoveException(ExceptionCode.DIRTY,"Unconsistency. Destination written but could not truncate source")

		last = int(len_tot.split(' ')[0])
		first = last- num_lines+1
		#print first		
		sed = subprocess.Popen(['sed','-i', str(first)+','+str(last)+'d',src_file_name],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		data, err_del = sed.communicate()
		
		if err_del:
			print err_del
			raise MoveException(ExceptionCode.DIRTY,"Unconsistency. Destination written but could not truncate source")			

		
		return None

	def writeLinesAtStart(self,request):
		file_name = request.file_name
		data = '\\n'.join(request.data)
		print file_name
		data= data[:-2]
		sed = subprocess.Popen(['sed','-i', '1i'+data,file_name],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
		data, err_prep = sed.communicate()
		if err_prep:
			raise WriteException(ExceptionCode.COULD_NOT_WRITE,"Could Not write to destination")
		return 		
