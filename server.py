#server.py
import sys
sys.path.append('gen-py')

from cloudbash import FileManagementService
from fileManagementHandler import FileManagementHandler 
																				
from thrift.transport import TSocket
from thrift.protocol import TCompactProtocol
from thrift.server import TServer
from thrift.transport import TTransport

class Server():
	def __init__(self,host,port):
		self.port = port
		self.host = host
		return
	def run(self):
		handler = FileManagementHandler()
		processor = FileManagementService.Processor(handler)
		transport = TSocket.TServerSocket(self.host,self.port)
		transport_factory = TTransport.TFramedTransportFactory()
		protocol_factory = TCompactProtocol.TCompactProtocolFactory()
		server = TServer.TSimpleServer(processor,transport,transport_factory,protocol_factory)
		print 'Starting server'
		try:
			server.serve()
		except Exception as e:
			print "Could not start server"
			print e

port = int(sys.argv[1])
server = Server('localhost',port)
server.run()